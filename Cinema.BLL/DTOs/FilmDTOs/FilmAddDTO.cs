﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.BLL.DTOs.FilmDTOs
{
	public class FilmAddDTO
	{
		public string Name { get; set; }
		public DateTime Duration { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime ExpirationDate { get; set; }
		public string RentalCompany { get; set; }
		public byte[] Image { get; set; }
	}
}
