﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.BLL.DTO
{
	public class EmployeePositionDTO
	{
		public Guid Id { get; set; }
		public string PositionName { get; set; }
	}
}
