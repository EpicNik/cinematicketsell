﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.BLL.DTOs.EmployeePositionDTOs
{
	public class EmployeePositionAddDTO
	{
		public string PositionName { get; set; }
	}
}
