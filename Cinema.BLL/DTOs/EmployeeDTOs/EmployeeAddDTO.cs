﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.BLL.DTOs.EmployeeDTOs
{
	public class EmployeeAddDTO
	{
		public string Name { get; set; }
		public int Age { get; set; }
		public Guid? EmployeePositionId { get; set; }
	}
}
