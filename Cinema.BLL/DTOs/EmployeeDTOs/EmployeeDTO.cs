﻿using Cinema.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.BLL.DTOs.EmployeeDTOs
{
	public class EmployeeDTO
	{
		public string Name { get; set; }
		public int Age { get; set; }
		public virtual EmployeePositionDTO EmployeePosition { get; set; }
	}
}
