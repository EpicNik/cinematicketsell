﻿using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Services
{
	public class EmployeeService : IEmployeeService
	{
		private readonly IUnitOfWork _unitOfWork;
		public EmployeeService(IUnitOfWork unitOfWork)
		{
			this._unitOfWork = unitOfWork;
		}

		public async Task<Employee> AddEmployee(Employee employee)
		{
			return await _unitOfWork.Employees.Add(employee);
		}

		public async Task<IEnumerable<Employee>> GetAllEmployee()
		{
			return await _unitOfWork.Employees.GetAll().ToListAsync();
		}

		public async Task<Employee> GetEmployeeById(Guid id)
		{
			return await _unitOfWork.Employees.GetById(id);
		}
	}
}
