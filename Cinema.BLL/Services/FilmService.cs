﻿using AutoMapper;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Services
{
	public class FilmService : IFilmService
	{
		private readonly IUnitOfWork _unitOfWork;
		public FilmService(IUnitOfWork unitOfWork)
		{
			this._unitOfWork = unitOfWork;
		}

		public async Task<Film> AddFilm(Film film)
		{
			return await _unitOfWork.Films.Add(film);
		}

		public async Task<Film> DeleteFilm(Guid id)
		{
			return await _unitOfWork.Films.Delete(id);
		}

		public async Task<IEnumerable<Film>> GetAllFilms()
		{
			return await _unitOfWork.Films.GetAll().ToListAsync();
		}

		public async Task<Film> GetById(Guid id)
		{
			return await _unitOfWork.Films.GetById(id);
		}

		public async Task<Film> UpdateFilm(Film film)
		{
			return await _unitOfWork.Films.Update(film);
		}

		public async Task<Film> UpdateFilmImage(IFormFile image)
		{
			using (var ms = new MemoryStream())
			{
				var imageName = Path.GetFileName(image.FileName);
				//var imageExtension = Path.GetExtension(imageName);
				//var filmId = Guid.Parse(imageName.Remove(imageName.LastIndexOf(imageExtension), imageExtension.Length));
				var filmId = Guid.Parse(imageName.Split('.')[0]);
				image.CopyTo(ms);
				var film = await _unitOfWork.Films.GetById(filmId);
				film.Image = ms.ToArray();
				film = await _unitOfWork.Films.Update(film);
				return film;
			}
		}
	}
}
