﻿using Cinema.BLL.DTO;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Services
{
	public class EmployeePositionService : IEmployeePositionService
	{
		private readonly IUnitOfWork _unitOfWork;
		public EmployeePositionService(IUnitOfWork unitOfWork)
		{
			this._unitOfWork = unitOfWork;
		}

		public async Task<EmployeePosition> AddEmployeePosition(EmployeePosition employeePosition)
		{
			return await _unitOfWork.EmployeePositions.Add(employeePosition);
		}

		public async Task<IEnumerable<EmployeePosition>> GetAllEmployeePosition()
		{
			return await _unitOfWork.EmployeePositions.GetAll().ToListAsync();
		}
	}
}
