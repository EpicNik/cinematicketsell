﻿using Cinema.BLL.DTOs.EmployeeDTOs;
using Cinema.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Interfaces
{
	public interface IEmployeeService
	{
		Task<IEnumerable<Employee>> GetAllEmployee();
		Task<Employee> AddEmployee(Employee employee);
		Task<Employee> GetEmployeeById(Guid id);
	}
}
