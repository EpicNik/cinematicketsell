﻿using Cinema.BLL.DTO;
using Cinema.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Interfaces
{
	public interface IEmployeePositionService
	{
		Task<IEnumerable<EmployeePosition>> GetAllEmployeePosition();
		Task<EmployeePosition> AddEmployeePosition(EmployeePosition employeePosition);
	}
}
