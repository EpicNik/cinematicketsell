﻿using Cinema.DAL.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Interfaces
{
	public interface IFilmService
	{
		Task<IEnumerable<Film>> GetAllFilms();
		Task<Film> AddFilm(Film film);
		Task<Film> GetById(Guid id);
		Task<Film> UpdateFilm(Film film);
		Task<Film> UpdateFilmImage(IFormFile image);
		Task<Film> DeleteFilm(Guid id);
	}
}
