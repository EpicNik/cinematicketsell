﻿using AutoMapper;
using Cinema.BLL.DTO;
using Cinema.BLL.DTOs.EmployeePositionDTOs;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class EmployeePositionController : ControllerBase
	{
		private readonly IEmployeePositionService _employeePositionService;
		private readonly IMapper _mapper;
		public EmployeePositionController(IEmployeePositionService employeePositionService, IMapper mapper)
		{
			this._employeePositionService = employeePositionService;
			this._mapper = mapper;
		}
		[HttpGet("GetAll")]
		public async Task<ActionResult<IEnumerable<EmployeePositionDTO>>> GetAllEmployeePosition()
		{
			var employeePosition = await _employeePositionService.GetAllEmployeePosition();
			var employeePositionDTO = _mapper.Map<IEnumerable<EmployeePositionDTO>>(employeePosition);
			return Ok(employeePositionDTO);
		}
		[HttpPost("CreateEmployeePosition")]
		public async Task<ActionResult> AddEmployeePosition([FromBody] EmployeePositionAddDTO employeePositionDTO)
		{
			var employeePosition = _mapper.Map<EmployeePosition>(employeePositionDTO);
			await _employeePositionService.AddEmployeePosition(employeePosition);
			return Ok();
		}
	}
}
