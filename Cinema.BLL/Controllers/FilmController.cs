﻿using AutoMapper;
using Cinema.BLL.DTOs.FilmDTOs;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class FilmController : ControllerBase
	{
		private readonly IFilmService _filmService;
		private readonly IMapper _mapper;
		public FilmController(IFilmService filmService, IMapper mapper)
		{
			_filmService = filmService;
			_mapper = mapper;
		}
		[HttpGet("GetAll")]
		public async Task<ActionResult<IEnumerable<FilmDTO>>> GetAllFilms()
		{
			var films = await _filmService.GetAllFilms();
			var filmsDTO = _mapper.Map<IEnumerable<FilmDTO>>(films);
			return Ok(filmsDTO);
		}
		[HttpPost("CreateFilm")]
		public async Task<ActionResult<Guid>> AddFilm([FromBody]FilmAddDTO filmAddDTO)
		{
			var film = _mapper.Map<Film>(filmAddDTO);
			film = await _filmService.AddFilm(film);
			return Ok(film.Id);
		}
		[HttpPost("UploadImage")]
		public async Task<ActionResult> UploadImageFilm(IFormFile image)
		{
			if (image != null)
			{
				if (image.Length > 0)
				{
					await _filmService.UpdateFilmImage(image);
				}
			}
			return Ok();
		}
		[HttpDelete("DeleteFilm")]
		public async Task<ActionResult> DeleteFilm([FromQuery] Guid id)
		{
			await _filmService.DeleteFilm(id);
			return Ok();
		}
	}
}
