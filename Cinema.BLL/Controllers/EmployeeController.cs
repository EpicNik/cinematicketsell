﻿using AutoMapper;
using Cinema.BLL.DTOs.EmployeeDTOs;
using Cinema.BLL.Interfaces;
using Cinema.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.BLL.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class EmployeeController:ControllerBase
	{
		private readonly IEmployeeService _employeeService;
		private readonly IMapper _mapper; 
		public EmployeeController(IEmployeeService employeeService, IMapper mapper)
		{
			this._employeeService = employeeService;
			this._mapper = mapper;
		}
		[HttpGet("GetAll")]
		public async Task<ActionResult<IEnumerable<EmployeeDTO>>> GetAllEmployee()
		{
			var employee = await _employeeService.GetAllEmployee();
			var employeeDTO = _mapper.Map<IEnumerable<EmployeeDTO>>(employee);
			return Ok(employeeDTO);
		}
		[HttpPost("CreateEmployee")]
		public async Task<ActionResult> AddEmployee(EmployeeAddDTO employeeAddDTO)
		{
			var employee = _mapper.Map<Employee>(employeeAddDTO);
			await _employeeService.AddEmployee(employee);
			return Ok(); 
		}
		[HttpGet("GetById")]
		public async Task<ActionResult<EmployeeDTO>> GetEmployeeById([FromQuery]Guid id)
		{
			var employee = await _employeeService.GetEmployeeById(id);
			var employeeDTO = _mapper.Map<EmployeeDTO>(employee);
			return Ok(employeeDTO);
		}
	}
}
