import React, { Component } from 'react';
import { Form, FormGroup, Input, Label, Jumbotron, Button } from 'reactstrap';

export class AddFilm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filmName: "",
            startDate: "",
            startTime: "",
            expirationDate: "",
            expirationTime: "",
            rentalCompany: "",
            file: {}
        }
        this.addFilmServ = this.addFilmServ.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
    }
    addFilmServ(event) {
        event.preventDefault();
        // console.log(this.state.file)
        if (this.state.filmName != "" && this.state.rentalCompany != "" && this.state.startDate != "" && this.state.startTime != "" && this.state.expirationDate != "" && this.state.expirationTime != "") {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    Name: this.state.filmName,
                    Duration: new Date(new Date(this.state.expirationDate + " " + this.state.expirationTime) - new Date(this.state.startDate + " " + this.state.startTime)),
                    StartDate: new Date(this.state.startDate + " " + this.state.startTime),
                    ExpirationDate: new Date(this.state.expirationDate + " " + this.state.expirationTime),
                    RentalCompany: this.state.rentalCompany,
                    Image: formData
                })
            };
            fetch('api/Film/CreateFilm', requestOptions)
                .then(response => response.json())
                .then(data => {
                })
                .catch(err => {
                    console.log(err);
                    console.log("Server Error");
                })
            this.props.closeForm();
        } else {
            alert("Form is invalid");
        }
        const formData = new FormData();
        formData.append('Image', this.state.file);
        fetch('api/Film/UploadImage', {
            method: 'post',
            body: {
                Image: JSON.stringify(formData)
            }
        })
            .then(data => {
                console.log(data)
            })
            .catch(err => {
                console.log(err);
                console.log("Server Error");
            })
    }
    handleChange(event) {
        const { name, value } = event.target
        this.setState({
            [name]: value
        })
    }
    handleChangeImage(event) {
        this.setState({ file: event.target.files[0] })
    }
    render() {
        return (
            <Jumbotron>
                <Form>
                    <FormGroup>
                        <Label for="NameInput">Name</Label>
                        <Input
                            id="NameInput"
                            placeholder="Film name"
                            name="filmName"
                            onChange={this.handleChange}
                            value={this.state.filmName}
                            valid={this.state.filmName != "" ? true : false}
                            invalid={this.state.filmName == "" ? true : false}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="StartDate">Start Date</Label>
                        <Input
                            type="date"
                            name="startDate"
                            id="StartDate"
                            placeholder="Date start"
                            onChange={this.handleChange}
                            value={this.state.startDate}
                            valid={this.state.startDate != "" ? true : false}
                            invalid={this.state.startDate == "" ? true : false}
                        />
                        <Input
                            type="time"
                            name="startTime"
                            id="StartTime"
                            placeholder="Time start"
                            onChange={this.handleChange}
                            value={this.state.startTime}
                            valid={this.state.startTime != "" ? true : false}
                            invalid={this.state.startTime == "" ? true : false}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="ExpirationDate">Expiration Date</Label>
                        <Input
                            type="date"
                            name="expirationDate"
                            id="ExpirationDate"
                            placeholder="Date expiration"
                            onChange={this.handleChange}
                            value={this.state.expirationDate}
                            valid={this.state.expirationDate != "" ? true : false}
                            invalid={this.state.expirationDate == "" ? true : false}
                        />
                        <Input
                            type="time"
                            name="expirationTime"
                            id="ExpirationTime"
                            placeholder="Time expiration"
                            onChange={this.handleChange}
                            value={this.state.expirationTime}
                            valid={this.state.expirationTime != "" ? true : false}
                            invalid={this.state.expirationTime == "" ? true : false}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="RentalCompany">Rental Company</Label>
                        <Input
                            id="RentalCompany"
                            placeholder="Rental Company"
                            name="rentalCompany"
                            onChange={this.handleChange}
                            value={this.state.rentalCompany}
                            valid={this.state.rentalCompany != "" ? true : false}
                            invalid={this.state.rentalCompany == "" ? true : false}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleFile">File</Label>
                        <Input
                            type="file"
                            name="file"
                            id="exampleFile"
                            onChange={this.handleChangeImage}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Button color="primary" onClick={this.addFilmServ}>Add film</Button>
                        <Button color="danger" onClick={() => this.props.closeForm()} className="float-right">Close</Button>
                    </FormGroup>
                </Form>
            </Jumbotron>
        )
    }
}