import React, { Component } from 'react';
import { Spinner, Button, Row, Media } from 'reactstrap';
import '../custom.css'
import { AddFilm } from './AddFilm'

export class Films extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            films: [],
            showAddFilmForm: false
        }
        this.getFilms = this.getFilms.bind(this);
        this.closeAddFilmForm = this.closeAddFilmForm.bind(this);
        this.deleteFilm = this.deleteFilm.bind(this);
    }
    getFilms() {
        this.setState({ isLoading: true });
        fetch('api/Film/GetAll')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ isLoading: false, films: data });
            })
            .catch(err => {
                console.log(err);
                console.log("Server Error");
                this.setState({ result: null, isLoading: false })
            })
    }
    closeAddFilmForm() {
        this.setState({ showAddFilmForm: false });
    }
    deleteFilm(id) {
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        };
        fetch('api/Film/DeleteFilm?id=' + id, requestOptions)
            .then(data => {
                this.setState({ films: this.state.films.filter(x => x.id != id) })
            })
            .catch(err => {
                console.log(err);
                console.log("Server Error");
            })
    }
    render() {
        let loader = this.state.isLoading ? <Spinner color="primary" /> : "";
        let films = this.state.films != [] ? this.state.films.map(f => <Row key={f.id}>
            <Media>
                <Media left href="#">
                    <Media object src={f.image != "" ? "data:image/png; base64," + f.image : "https://img.icons8.com/carbon-copy/100/000000/image.png"} />
                </Media>
                <Media body>
                    <Media heading>
                        {f.name}
                    </Media>
                    {f.rentalCompany}<br />
                    <img src="https://img.icons8.com/android/24/000000/trash.png" style={{ cursor: 'pointer' }} onClick={() => this.deleteFilm(f.id)} />
                    <br />
                    <Button color='primary' style={{ marginTop: '5px' }} onClick={() => alert("Билет куплен")}>Купить</Button>
                </Media>
            </Media>
        </Row>) : "";
        let addFilm = this.state.showAddFilmForm ? <AddFilm
            closeForm={this.closeAddFilmForm}
        /> : "";
        return (
            <div>
                <Row>
                    <Button color='primary' onClick={this.getFilms}>Get all films</Button> {' '}
                    <Button color='success' onClick={() => this.setState({ showAddFilmForm: !this.state.showAddFilmForm })}>Add Film</Button>
                </Row>
                <Row>
                    {addFilm}
                </Row>
                {films}
                {loader}
            </div>
        )
    }
}