import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import {Films} from './components/Films'

import './custom.css'

export default () => (
    <Layout>
        {/* <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
        <Route path='/fetch-data/:startDateIndex?' component={FetchData} /> */}
        <Route path='/Films' component={Films} />
    </Layout>
);
