﻿using Cinema.DAL.EF;
using Cinema.DAL.EF.Repositories;
using Cinema.DAL.Interfaces;
using Cinema.DAL.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Extensions
{
	public static class DBDependencies
	{
		public static void AddCinemaDependencies(this IServiceCollection services)
		{
			//services.AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
			services.AddTransient<IEmployeeRepository, EmployeeRepository>();
			services.AddTransient<IEmployeePositionRepository, EmployeePositionRepository>();
			services.AddTransient<IFilmRepository, FilmRepository>();
			services.AddTransient<IHallRepository, HallRepository>();
			services.AddTransient<IHallSectorRepository, HallSectorRepository>();
			services.AddTransient<IPlaceRepository, PlaceRepository>();
			services.AddTransient<ISessionRepository, SessionRepository>();
			services.AddTransient<ITicketMovementRepository, TicketMovementRepository>();
			services.AddTransient<ITicketPriceRepository, TicketPriceRepository>();
			services.AddTransient<ITicketRepository, TicketRepository>();
			services.AddTransient<IUnitOfWork, UnitOfWork>();
		}
	}
}
