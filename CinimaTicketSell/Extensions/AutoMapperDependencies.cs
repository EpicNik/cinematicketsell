﻿using AutoMapper;
using CinemaTicketSell.Mapping;
using CinimaTicketSell;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Extensions
{
	public static class AutoMapperDependencies
	{
		public static void AddAutoMapperDependencies(this IServiceCollection services)
		{
			services.AddAutoMapper(typeof(EmployeePositionMap));
			services.AddAutoMapper(typeof(EmployeeMap));
			services.AddAutoMapper(typeof(FilmMap));
		}
	}
}
