﻿using Cinema.BLL.Interfaces;
using Cinema.BLL.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Extensions
{
	public static class ServicesDependencies
	{
		public static void AddServicesDependencies(this IServiceCollection services)
		{
			services.AddTransient<IEmployeePositionService, EmployeePositionService>();
			services.AddTransient<IEmployeeService, EmployeeService>();
			services.AddTransient<IFilmService, FilmService>();
		}
	}
}
