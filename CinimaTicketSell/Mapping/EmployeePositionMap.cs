﻿using AutoMapper;
using Cinema.BLL.DTO;
using Cinema.BLL.DTOs.EmployeePositionDTOs;
using Cinema.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Mapping
{
	public class EmployeePositionMap:Profile
	{
		public EmployeePositionMap()
		{
			CreateMap<EmployeePosition, EmployeePositionDTO>()
				.ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.PositionName))
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id)).ReverseMap();
			CreateMap<EmployeePosition, EmployeePositionAddDTO>().ReverseMap();
		}
	}
}
