﻿using AutoMapper;
using Cinema.BLL.Controllers;
using Cinema.BLL.DTOs.FilmDTOs;
using Cinema.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Mapping
{
	public class FilmMap:Profile
	{
		public FilmMap()
		{
			CreateMap<Film, FilmDTO>().ReverseMap();
			CreateMap<Film, FilmAddDTO>().ReverseMap();
		}
	}
}
