﻿using AutoMapper;
using Cinema.BLL.DTO;
using Cinema.BLL.DTOs.EmployeeDTOs;
using Cinema.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaTicketSell.Mapping
{
	public class EmployeeMap : Profile
	{
		public EmployeeMap()
		{
			CreateMap<Employee, EmployeeDTO>().ReverseMap();
			CreateMap<Employee, EmployeeAddDTO>().ReverseMap();
		}
	}
}
