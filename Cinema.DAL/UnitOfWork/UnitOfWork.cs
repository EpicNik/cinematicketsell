﻿using Cinema.DAL.EF;
using Cinema.DAL.EF.Repositories;
using Cinema.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.UnitOfWork
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly CinemaDBContext _context;
		public UnitOfWork(CinemaDBContext context)
		{
			this._context = context;
			EmployeePositions = new EmployeePositionRepository(_context);
			Employees = new EmployeeRepository(_context);
			Films = new FilmRepository(_context);
			Halls = new HallRepository(_context);
			HallSectors = new HallSectorRepository(_context);
			Places = new PlaceRepository(_context);
			Sessions = new SessionRepository(_context);
			TicketMovements = new TicketMovementRepository(_context);
			TicketPrices = new TicketPriceRepository(_context);
			Tickets = new TicketRepository(_context);
		}

		public IEmployeePositionRepository EmployeePositions { get; private set; }

		public IEmployeeRepository Employees { get; private set; }

		public IFilmRepository Films { get; private set; }

		public IHallRepository Halls { get; private set; }

		public IHallSectorRepository HallSectors { get; private set; }

		public IPlaceRepository Places { get; private set; }

		public ISessionRepository Sessions { get; private set; }

		public ITicketMovementRepository TicketMovements { get; private set; }

		public ITicketPriceRepository TicketPrices { get; private set; }

		public ITicketRepository Tickets { get; private set; }

		public void Dispose()
		{
			_context.Dispose();
		}
	}
}
