﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class Hall : BaseEntity
	{
		[Required]
		[StringLength(64)]
		public string Name { get; set; }
		[Required]
		public int PlacesCount { get; set; }
		[StringLength(512)]
		public string Description { get; set; }
		[Required]
		public int RowsNumber { get; set; }
		[Required]
		public int PlacesNumber { get; set; }


		public IEnumerable<Session> Sessions { get; set; }
		public IEnumerable<Place> Places { get; set; }
		public IEnumerable<HallSector> HallSectors { get; set; }
	}
}