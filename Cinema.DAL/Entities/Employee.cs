﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Cinema.DAL.Entities
{
	public class Employee : BaseEntity
	{

		[Required]
		[StringLength(64)]
		public string Name { get; set; }
		[Required]
		public int Age { get; set; }



		public Guid? EmployeePositionId { get; set; }



		public EmployeePosition EmployeePosition { get; set; }



		public IEnumerable<TicketMovement> TicketMovements { get; set; }

	}
}