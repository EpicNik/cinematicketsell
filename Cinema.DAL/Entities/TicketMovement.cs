﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cinema.DAL.Entities
{
	public class TicketMovement : BaseEntity
	{
		[Required]
		public DateTime DateOperation { get; set; }
		[Required]
		[StringLength(512)]
		public string Operation { get; set; }


		public Guid? EmployeeId { get; set; }
		public Guid? TicketId { get; set; }


		public Ticket Ticket { get; set; }
		public Employee Employee { get; set; }
	}
}
