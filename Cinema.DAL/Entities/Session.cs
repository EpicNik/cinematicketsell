﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class Session : BaseEntity
	{
		[Required]
		public DateTime DateSession { get; set; }


		public Guid? FilmId { get; set; }
		public Guid? HallId { get; set; }


		public Hall Hall { get; set; }
		public Film Film { get; set; }
		public IEnumerable<Ticket> Tickets { get; set; }
		public IEnumerable<TicketPrice> TicketPrices { get; set; }
	}
}