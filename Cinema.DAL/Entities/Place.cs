﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class Place : BaseEntity
	{
		[Required]
		public int RowNumber { get; set; }
		[Required]
		public int PlaceNumber { get; set; }


		public Guid? HallId { get; set; }


		public Hall Hall { get; set; }
		public IEnumerable<Ticket> Tickets { get; set; }

	}
}