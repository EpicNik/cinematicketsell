﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class HallSector : BaseEntity
	{
		[Required]
		[StringLength(64)]
		public string Name { get; set; }
		[StringLength(512)]
		public string Description { get; set; }
		[Required]
		public int FirstRow { get; set; }
		[Required]
		public int LastRow { get; set; }


		public Guid? HallId { get; set; }


		public Hall Hall { get; set; }
		public IEnumerable<TicketPrice> TicketPrices { get; set; }

	}
}