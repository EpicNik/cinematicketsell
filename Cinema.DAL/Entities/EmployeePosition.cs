﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class EmployeePosition : BaseEntity
	{
		[Required]
		[StringLength(128)]
		public string PositionName { get; set; }


		public IEnumerable<Employee> Employees { get; set; }
	}
}