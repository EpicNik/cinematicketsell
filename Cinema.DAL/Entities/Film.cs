﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cinema.DAL.Entities
{
	public class Film : BaseEntity
	{
		[Required]
		[StringLength(64)]
		public string Name { get; set; }
		[Required]
		public DateTime Duration { get; set; }
		[Required]
		public DateTime StartDate { get; set; }
		[Required]
		public DateTime ExpirationDate { get; set; }
		[Required]
		[StringLength(256)]
		public string RentalCompany { get; set; }
		[MaxLength]
		public byte[] Image { get; set; }


		public IEnumerable<Session> Sessions { get; set; }

	}
}
