﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class TicketPrice : BaseEntity
	{
		[Required]
		public double Price { get; set; }


		public Guid? SessionId { get; set; }
		public Guid? HallSectorId { get; set; }


		public Session Session { get; set; }
		public HallSector HallSector { get; set; }
	}
}