﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cinema.DAL.Entities
{
	public class Ticket : BaseEntity
	{
		[Required]
		public DateTime ReleaseDate { get; set; }
		[Required]
		[DefaultValue(false)]
		public bool IsPaid { get; set; }
		[Required]
		[DefaultValue(false)]
		public bool IsReservation { get; set; }
		[Required]
		[DefaultValue(false)]
		public bool IsDestroyed { get; set; }


		public Guid? SessionId { get; set; }
		public Guid? PlaceId { get; set; }



		public Session Session { get; set; }
		public Place Place { get; set; }
		public IEnumerable<TicketMovement> TicketMovements { get; set; }
	}
}