﻿using System;

namespace Cinema.DAL.Interfaces
{
	public interface IBaseEntity
	{
		Guid Id { get; set; }
		DateTime? CreatedOn { get; set; }
		DateTime? ModifiedOn { get; set; }
	}
}