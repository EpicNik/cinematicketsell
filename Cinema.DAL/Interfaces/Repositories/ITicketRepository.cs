﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface ITicketRepository:IGenericRepository<Ticket>
	{
		
	}
}