﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface ITicketMovementRepository:IGenericRepository<TicketMovement>
	{
		
	}
}