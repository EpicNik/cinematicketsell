﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IEmployeePositionRepository: IGenericRepository<EmployeePosition>
	{
		
	}
}