﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IHallSectorRepository : IGenericRepository<HallSector>
	{

	}
}