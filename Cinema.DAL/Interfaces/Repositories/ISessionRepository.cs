﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface ISessionRepository:IGenericRepository<Session>
	{
		
	}
}