﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IPlaceRepository:IGenericRepository<Place>
	{
		
	}
}