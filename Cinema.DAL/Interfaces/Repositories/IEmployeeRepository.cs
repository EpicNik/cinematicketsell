﻿using Cinema.DAL.EF;
using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IEmployeeRepository : IGenericRepository<Employee>
	{
	}
}