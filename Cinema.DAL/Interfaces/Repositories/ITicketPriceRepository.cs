﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface ITicketPriceRepository : IGenericRepository<TicketPrice>
	{

	}
}