﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IFilmRepository:IGenericRepository<Film>
	{
		
	}
}