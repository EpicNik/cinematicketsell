﻿using Cinema.DAL.Entities;

namespace Cinema.DAL.Interfaces
{
	public interface IHallRepository: IGenericRepository<Hall>
	{
		
	}
}