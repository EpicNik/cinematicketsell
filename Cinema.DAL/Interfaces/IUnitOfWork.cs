﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cinema.DAL.Interfaces
{
	public interface IUnitOfWork:IDisposable
	{
		IEmployeePositionRepository EmployeePositions { get; }
		IEmployeeRepository Employees { get; }
		IFilmRepository Films { get; }
		IHallRepository Halls { get; }
		IHallSectorRepository HallSectors { get; }
		IPlaceRepository Places { get; }
		ISessionRepository Sessions { get; }
		ITicketMovementRepository TicketMovements { get; }
		ITicketPriceRepository TicketPrices { get; }
		ITicketRepository Tickets { get; }
	}
}
