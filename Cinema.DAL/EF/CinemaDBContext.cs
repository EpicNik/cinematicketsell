﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Cinema.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Bson;

namespace Cinema.DAL.EF
{
	public class CinemaDBContext : DbContext
	{
		public CinemaDBContext(DbContextOptions<CinemaDBContext> options) : base(options)
		{
		}

		public DbSet<Employee> Employees { get; set; }
		public DbSet<EmployeePosition> EmployeePositions { get; set; }
		public DbSet<Film> Films { get; set; }
		public DbSet<Hall> Halls { get; set; }
		public DbSet<HallSector> HallSectors { get; set; }
		public DbSet<Place> Places { get; set; }
		public DbSet<Session> Sessions { get; set; }
		public DbSet<Ticket> Tickets { get; set; }
		public DbSet<TicketMovement> TicketMovements { get; set; }
		public DbSet<TicketPrice> TicketPrices { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>()
				.HasOne<EmployeePosition>(s => s.EmployeePosition)
				.WithMany(g => g.Employees)
				.HasForeignKey(s => s.EmployeePositionId);
			modelBuilder.Entity<TicketMovement>()
				.HasOne<Employee>(s => s.Employee)
				.WithMany(g => g.TicketMovements)
				.HasForeignKey(s => s.EmployeeId);
			modelBuilder.Entity<TicketMovement>()
				.HasOne<Ticket>(s => s.Ticket)
				.WithMany(g => g.TicketMovements)
				.HasForeignKey(s => s.TicketId);
			modelBuilder.Entity<Ticket>()
				.HasOne<Session>(s => s.Session)
				.WithMany(g => g.Tickets)
				.HasForeignKey(s => s.SessionId);
			modelBuilder.Entity<Ticket>()
				.HasOne<Place>(s => s.Place)
				.WithMany(g => g.Tickets)
				.HasForeignKey(s => s.PlaceId);
			modelBuilder.Entity<TicketPrice>()
				.HasOne<Session>(s => s.Session)
				.WithMany(g => g.TicketPrices)
				.HasForeignKey(s => s.SessionId);
			modelBuilder.Entity<TicketPrice>()
				.HasOne<HallSector>(s => s.HallSector)
				.WithMany(g => g.TicketPrices)
				.HasForeignKey(s => s.HallSectorId);
			modelBuilder.Entity<Session>()
				.HasOne<Film>(s => s.Film)
				.WithMany(g => g.Sessions)
				.HasForeignKey(s => s.FilmId);
			modelBuilder.Entity<Session>()
				.HasOne<Hall>(s => s.Hall)
				.WithMany(g => g.Sessions)
				.HasForeignKey(s => s.HallId);
			modelBuilder.Entity<Place>()
				.HasOne<Hall>(s => s.Hall)
				.WithMany(g => g.Places)
				.HasForeignKey(s => s.HallId);
			modelBuilder.Entity<HallSector>()
				.HasOne<Hall>(s => s.Hall)
				.WithMany(g => g.HallSectors)
				.HasForeignKey(s => s.HallId);
		}

	}
}
