﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DAL.EF.Repositories
{
	public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
	{
		public EmployeeRepository(CinemaDBContext context) : base(context)
		{

		}
		public override IQueryable<Employee> GetAll()
		{
			IQueryable<Employee> employees = base.GetAll().Include(entity => entity.EmployeePosition);
			return employees;
		}
		public override async Task<Employee> GetById(Guid id)
		{
			IQueryable<Employee> employee = base.GetAll().Include(entity => entity.EmployeePosition);
			return await employee.FirstOrDefaultAsync(entity => entity.Id == id);
		}
		// We can add new methods specific to the movie repository here in the future
	}
}