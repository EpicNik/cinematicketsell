﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class TicketMovementRepository : GenericRepository<TicketMovement>, ITicketMovementRepository
	{
		public TicketMovementRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
