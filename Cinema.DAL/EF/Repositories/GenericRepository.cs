﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cinema.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.DAL.EF
{
	public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IBaseEntity
	{
		private readonly CinemaDBContext _context;

		public GenericRepository(CinemaDBContext context)
		{
			_context = context;
		}
		public virtual IQueryable<TEntity> GetAll()
		{
			return _context.Set<TEntity>();
		}

		public virtual async Task<TEntity> GetById(Guid id)
		{
			return await _context.Set<TEntity>().FirstOrDefaultAsync(entity => entity.Id == id);
		}

		public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> expression)
		{
			return _context.Set<TEntity>().Where(expression);
		}

		public async Task<TEntity> Add(TEntity entity)
		{
			entity.CreatedOn = DateTime.Now;
			_context.Set<TEntity>().Add(entity);
			await _context.SaveChangesAsync();
			return entity;
		}

		public async Task<TEntity> Update(TEntity entity)
		{
			_context.Entry(entity).State = EntityState.Modified;
			entity.ModifiedOn = DateTime.Now;
			await _context.SaveChangesAsync();
			return entity;
		}

		public async Task<TEntity> Delete(Guid id)
		{
			var entity = await _context.Set<TEntity>().FindAsync(id);
			if (entity == null)
			{
				return entity;
			}

			_context.Set<TEntity>().Remove(entity);
			await _context.SaveChangesAsync();
			return entity;
		}
	}
}