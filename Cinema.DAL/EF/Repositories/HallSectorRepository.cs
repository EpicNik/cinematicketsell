﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class HallSectorRepository : GenericRepository<HallSector>, IHallSectorRepository
	{
		public HallSectorRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
