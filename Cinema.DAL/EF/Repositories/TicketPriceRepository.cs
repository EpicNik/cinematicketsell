﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class TicketPriceRepository : GenericRepository<TicketPrice>, ITicketPriceRepository
	{
		public TicketPriceRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
