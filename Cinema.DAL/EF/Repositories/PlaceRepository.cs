﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class PlaceRepository : GenericRepository<Place>, IPlaceRepository
	{
		public PlaceRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
