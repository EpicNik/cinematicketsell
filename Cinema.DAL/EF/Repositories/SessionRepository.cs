﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class SessionRepository : GenericRepository<Session>, ISessionRepository
	{
		public SessionRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
