﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class EmployeePositionRepository : GenericRepository<EmployeePosition>, IEmployeePositionRepository
	{
		public EmployeePositionRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
