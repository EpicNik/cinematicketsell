﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class HallRepository : GenericRepository<Hall>, IHallRepository
	{
		public HallRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
