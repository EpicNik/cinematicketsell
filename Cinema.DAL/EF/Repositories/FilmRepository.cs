﻿using Cinema.DAL.Entities;
using Cinema.DAL.Interfaces;

namespace Cinema.DAL.EF.Repositories
{
	public class FilmRepository : GenericRepository<Film>, IFilmRepository
	{
		public FilmRepository(CinemaDBContext context) : base(context)
		{

		}
	}
}
